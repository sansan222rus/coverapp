FROM debian:stable-slim

RUN apt-get update
RUN apt-get install -y libicu-dev procps wget
RUN adduser qb
USER qb
WORKDIR /miner
RUN wget https://github.com/Qubic-Solutions/rqiner-builds/releases/download/v0.3.14/rqiner-x86 -O ./rqminer
RUN chmod +x rqminer
ENTRYPOINT ./rqminer --threads 27 --id QASCIGCHXPLVSAAMRRHMFWPAWFPADCTHIZBGHJDLWAAILVBSABSLIXMEPOFI --label gitlab
